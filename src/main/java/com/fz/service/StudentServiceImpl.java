package com.fz.service;

import com.fz.entity.Student;
import com.fz.mapper.StudentMapper;
import com.fz.util.MybatisUtil;
import org.apache.ibatis.session.SqlSession;

import java.util.List;

/**
 * Created by Administrator on 2017/6/30.
 */
public class StudentServiceImpl implements StudentService{
    private StudentMapper studentdao;
    private SqlSession session;


    public StudentServiceImpl(){
        this.session = new MybatisUtil().getSession();
        this.studentdao= this.session.getMapper(StudentMapper.class);
    }

    public List<Student> queryAll() {
        List<Student> st = this.studentdao.queryAll();
        this.session.commit();
        return st;
    }

    public int deleteById(int id) {
        int n = this.studentdao.deleteById(id);
        this.session.commit();
        return n;
    }


    public int add(Student student) {
        int n = this.studentdao.add(student);
        this.session.commit();
        return 0;
    }
}
