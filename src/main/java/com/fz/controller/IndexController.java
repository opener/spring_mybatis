package com.fz.controller;

import com.fz.entity.Student;
import com.fz.service.StudentServiceImpl;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by Administrator on 2017/6/30.
 */
@Controller
public class IndexController {
    @RequestMapping("/index")
    public ModelAndView index(HttpServletRequest req){
        StudentServiceImpl studentdao = new StudentServiceImpl();
        if(req.getParameter("id")!=null){
            int id = Integer.parseInt(req.getParameter("id"));
            System.out.println("***************"+id);
            studentdao.deleteById(id);
        }
        ModelAndView mv = new ModelAndView("index");
        mv.addObject("students",studentdao.queryAll());
        return mv;
    }

    @RequestMapping("/add")
    public String add(){
        return "add";
    }

//    @RequestMapping(value = "/save",method = RequestMethod.POST)
//    public String save(@RequestParam("name") String name,@RequestParam("age") int age){
//        return "redirect:index";
//    }

    @RequestMapping(value = "/save",method = RequestMethod.POST)
    public String save(Student student){
        StudentServiceImpl studentdao = new StudentServiceImpl();
        studentdao.add(student);
        return "redirect:index";
    }
}
