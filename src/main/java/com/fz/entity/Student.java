package com.fz.entity;

import lombok.Data;

/**
 * Created by Administrator on 2017/6/30.
 */
@Data
public class Student {
    private int id;
    private String name;
    private int age;
}
