package com.fz.util;

import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

import java.io.IOException;
import java.io.InputStream;

/**
 * Created by Administrator on 2017/6/30.
 */
public class MybatisUtil {
    private SqlSessionFactory sf;
    private SqlSession ss;
    public MybatisUtil(){
        InputStream is = null;
        try {
            is = Resources.getResourceAsStream("mybatis-config.xml");
            this.sf = new SqlSessionFactoryBuilder().build(is);
            this.ss = this.sf.openSession();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public SqlSession getSession(){
        return this.ss;
    }
}
