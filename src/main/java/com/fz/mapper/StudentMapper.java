package com.fz.mapper;

import com.fz.entity.Student;

import java.util.List;

/**
 * Created by Administrator on 2017/6/30.
 */
public interface StudentMapper {
    public List<Student> queryAll();
    public int deleteById(int id);
    public int add(Student student);
}
