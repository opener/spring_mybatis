<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2017/6/30
  Time: 11:44
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta charset="utf-8">
    <title>客户管理</title>
    <link rel="stylesheet" href="css/index.css">
</head>
<body>
<table class="tt">
    <caption>客户管理</caption>
    <tr>
        <th>姓名</th>
        <th>年龄</th>
        <th>操作</th>
    </tr>
    <c:forEach items="${students}" var="s">
        <tr>
            <td>${s.name}</td>
            <td>${s.age}</td>
            <td><a href="?id=${s.id}">删除</a></td>
        </tr>
    </c:forEach>
    <tr>
        <td colspan="100"><a href="add">添加客户信息</a></td>
    </tr>
</table>
</body>
</html>
